# Stage 1
FROM microsoft/aspnetcore-build:1.1.2 AS builder
WORKDIR /source

# caches restore result by copying csproj file separately.  Copy the csproj file and run dotnet restore
COPY src/aspdocker/aspdocker.csproj ./src/aspdocker/
RUN dotnet restore ./src/aspdocker/aspdocker.csproj

COPY src/aspdocker.tests/aspdocker.tests.csproj ./src/aspdocker.tests/
RUN dotnet restore ./src/aspdocker.tests/aspdocker.tests.csproj

# copy the rest of the application and publish it to the /app folder
COPY . .

RUN dotnet test ./src/aspdocker.tests/aspdocker.tests.csproj

RUN dotnet publish ./src/aspdocker/aspdocker.csproj --output /app/ --configuration Release

# Stage 2
# Plain aspnetcore image and gets build artefacts from the app folder
FROM microsoft/aspnetcore:1.1.2
WORKDIR /app
COPY --from=builder /app .

# container listens on the stated 5000 port 
EXPOSE 5000/tcp
ENTRYPOINT ["dotnet", "aspdocker.dll"]
